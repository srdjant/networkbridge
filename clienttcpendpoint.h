/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#ifndef CLIENTTCPENDPOINT_H
#define CLIENTTCPENDPOINT_H

#include "endpoint.h"

#include <QHostAddress>
#include <QAbstractSocket>

class QString;
class QTcpSocket;

class ClientTCPEndpoint : public Endpoint
{
    Q_OBJECT

public:
    ClientTCPEndpoint();

    void start();
    void configure(const NetworkConfig &_config);

    public slots:
       virtual void toDevice(const QByteArray &);

    private slots:
       void obtainData();
       void handleError(QAbstractSocket::SocketError);

    private:
       QTcpSocket  *pSock;
       QHostAddress mHost;
       ushort       mPort;
};

#endif // CLIENTTCPENDPOINT_H
