/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QHash>
#include "ui_mainwindow.h"

#include "endpointmap.h"
class QWorkspace;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~MainWindow();

public slots:
    void newMapping();
    void deleteMapping(const QString &);
    void createNewViewer(const QString &);

private:
    Ui::MainWindowClass ui;
    QHash<QString, EndpointMap *> mConnectionHash;
    QWorkspace *pWorkspace;
};

#endif // MAINWINDOW_H
