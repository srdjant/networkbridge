/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#include "newmappingdialog.h"

#include "endpointfactory.h"

#include "endpoint.h"
#include "clienttcpendpoint.h"
#include "servertcpendpoint.h"

NewMappingDialog::NewMappingDialog(QDialog *parent) :
    QDialog(parent)
{
    ui.setupUi(this);

    QStringList endpointTypes;
    endpointTypes << "TCP Client" << "TCP Server";

    setEndpointTypes(endpointTypes);

    connect(ui.pEndpointAType, SIGNAL(activated(int)), this, SLOT(setAPage(int)));
    connect(ui.pEndpointBType, SIGNAL(activated(int)), this, SLOT(setBPage(int)));

    connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(formulateAndClose()));
    connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

const QString NewMappingDialog::getName() const
{
    return ui.pMappingName->text();
}

void NewMappingDialog::setEndpointTypes(QStringList &_types)
{
    ui.pEndpointAType->addItems(_types);
    ui.pEndpointBType->addItems(_types);
}

void NewMappingDialog::setAPage(int _page)
{
    if (_page == 1)  // server
    {
       ui.pClientHostA->setText("0.0.0.0");  // for any
       ui.pClientHostA->setEnabled(false);
    }
    else
    {
        ui.pClientHostA->setText("127.0.0.1");  // for localhost
        ui.pClientHostA->setEnabled(true);
    }
}

void NewMappingDialog::setBPage(int _page)
{
  if (_page == 1)  // server
  {
     ui.pClientHostB->setText("0.0.0.0");  // for any
     ui.pClientHostB->setEnabled(false);
  }
  else
  {
      ui.pClientHostB->setText("127.0.0.1");  // for localhost
      ui.pClientHostB->setEnabled(true);
  }
}

QList<NetworkConfig> &NewMappingDialog::getConfig()
{
    return mConfigList;
}

/* Currently should suffice for TCP/UDP client and server
   */
void NewMappingDialog::formulateAndClose()
{
    NetworkConfig configA;
    configA.type = ui.pEndpointAType->currentText();
    configA.host = ui.pClientHostA->text();
    configA.port = ui.pClientPortA->value();

    NetworkConfig configB;
    configB.type = ui.pEndpointBType->currentText();
    configB.host = ui.pClientHostB->text();
    configB.port = ui.pClientPortB->value();

    mConfigList.append(configA);
    mConfigList.append(configB);

    QDialog::accept();
}
