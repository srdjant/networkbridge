/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#include "servertcpendpoint.h"

#include <QTcpServer>
#include <QTcpSocket>

ServerTCPEndpoint::ServerTCPEndpoint()
    : mHost(QHostAddress::Any)
    , mPort(5000)
{
    pServerSock = new QTcpServer(this);

    connect(pServerSock, SIGNAL(newConnection()), this, SLOT(handleNewClient()));
}

void ServerTCPEndpoint::start()
{
    if (!pServerSock->listen(mHost, mPort))
    {
      qWarning("Unable to start the TCP server on port %d.", mPort);
    }
    else
        qWarning("TCP Server started on port: %d", mPort);
}

void ServerTCPEndpoint::configure(const NetworkConfig &_config)
{
    mHost = QHostAddress(_config.host);
    mPort = _config.port;
}

void ServerTCPEndpoint::handleNewClient()
{
    QTcpSocket *client = pServerSock->nextPendingConnection();
    connect(client, SIGNAL(disconnected()),
            client, SLOT(deleteLater()));

    connect(client, SIGNAL(readyRead()), this, SLOT(dataFromClient()));

    pConnectedDevice = client;
}

void ServerTCPEndpoint::dataFromClient()
{
   QTcpSocket *sock = dynamic_cast<QTcpSocket *> (sender());
   if (sock)
   {
       QByteArray buffer = sock->readAll();
       emit dataFromDevice(buffer);
   }
}

void ServerTCPEndpoint::toDevice(const QByteArray &_data)
{
    pConnectedDevice->write(_data);
}
