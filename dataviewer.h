/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#ifndef DATAVIEWER_H
#define DATAVIEWER_H

#include <QTextEdit>
class QByteArray;
class EndpointMap;

class DataViewer : public QTextEdit
{
Q_OBJECT
public:
    explicit DataViewer(QWidget *parent = 0);
    virtual ~DataViewer();

    void setMapper(EndpointMap *_mapper);

  // TODO when closing, want to de-register from the mapper
  //  virtual void closeEvent(QCloseEvent *event);

public slots:
    void appendFromSource(const QByteArray &);

private:
     EndpointMap *pMapper;
};

#endif // DATAVIEWER_H
