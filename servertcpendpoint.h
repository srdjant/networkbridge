/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#ifndef SERVERTCPENDPOINT_H
#define SERVERTCPENDPOINT_H

#include <QList>
#include <QHostAddress>

class QTcpServer;
class QTcpSocket;

#include "endpoint.h"

class ServerTCPEndpoint : public Endpoint
{
    Q_OBJECT
    public:
        ServerTCPEndpoint();

        void start();
        virtual void configure(const NetworkConfig &_config);

    public slots:
        virtual void toDevice(const QByteArray &);   // send to all connected devices

        virtual void handleNewClient();
        virtual void dataFromClient();

private:
        QTcpServer         *pServerSock;
        QList<QTcpSocket *> mClientList;
        
        QTcpSocket *pConnectedDevice;
        QHostAddress mHost;
        ushort       mPort;
};

#endif // SERVERTCPENDPOINT_H
