/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#ifndef NEWMAPPINGDIALOG_H
#define NEWMAPPINGDIALOG_H

#include <QDialog>
#include <QList>

#include "ui_newmappingdialog.h"

#include "endpoint.h"

class NewMappingDialog : public QDialog
{
Q_OBJECT
public:
    explicit NewMappingDialog(QDialog *parent = 0);

    void setEndpointTypes(QStringList &_types);

    QList<NetworkConfig> &getConfig();
    const QString getName() const;

public slots:
    void setAPage(int);
    void setBPage(int);
    void formulateAndClose();

private:
    Ui::NewMappingDialog ui;
    QList<NetworkConfig> mConfigList;
};

#endif // NEWMAPPINGDIALOG_H
