/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#include <QHostAddress>
#include <QWorkspace>

#include "mainwindow.h"
#include "newmappingdialog.h"
#include "endpointfactory.h"
#include "dataviewer.h"

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
    ui.setupUi(this);
    setWindowTitle("Network Bridge");

    pWorkspace = new QWorkspace();
    setCentralWidget(pWorkspace);

    connect(ui.action_New,  SIGNAL(activated()), this, SLOT(newMapping()));
    connect(ui.action_Quit, SIGNAL(activated()), this, SLOT(close()));
}

MainWindow::~MainWindow()
{
    QList<EndpointMap *> vals = mConnectionHash.values();
    for(int i = 0; i < vals.count(); ++i)
        delete vals.at(i);  // delete objects but don't remove from hash or list
    mConnectionHash.clear();
}

void MainWindow::newMapping()
{
    NewMappingDialog newMapDlg;
    if (newMapDlg.exec() == QDialog::Accepted)
    {
        QList<NetworkConfig> &conf = newMapDlg.getConfig();

        EndpointMap *map = new EndpointMap();
        map->setName(newMapDlg.getName());

        // TODO move object creation to endpoint map, in setup()
        Endpoint *ifaceA = EndpointFactory::createEndpoint(conf.at(0));
        Endpoint *ifaceB = EndpointFactory::createEndpoint(conf.at(1));
        map->addEndpointPair(ifaceA, ifaceB);
        map->setup();
        ifaceA->start();
        ifaceB->start();

        // eventually add Edit menu action
        QMenu *mapMenu = ui.menu_Mappings->addMenu(map->name());

        QAction *viewAction = mapMenu->addAction(tr("&View Data"));
        QAction *deleteAction = mapMenu->addAction(tr("&Delete Mapping"));

        connect(viewAction, SIGNAL(triggered()), map, SLOT(createViewWindow()));
        connect(deleteAction, SIGNAL(triggered()), map, SLOT(deleteAction()));
        connect(map, SIGNAL(deleteMapping(const QString &)),
                this, SLOT(deleteMapping(const QString &)));
        connect(map, SIGNAL(createNewViewer(const QString &)),
                this, SLOT(createNewViewer(const QString &)));

        map->appendMenu(mapMenu);

        mConnectionHash.insert(map->name() , map);
    }
}

void MainWindow::deleteMapping(const QString &_name)
{
    EndpointMap *map = mConnectionHash.take(_name);

    map->deleteLater();
}

void MainWindow::createNewViewer(const QString &_name)
{
    EndpointMap *map = mConnectionHash[_name];
    if (map)
    {
        DataViewer *view = new DataViewer(this);
        view->setMapper(map);
        pWorkspace->addWindow(view)->show();
    }
}

