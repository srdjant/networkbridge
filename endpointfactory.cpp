/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#include "endpointfactory.h"

#include "clienttcpendpoint.h"
#include "servertcpendpoint.h"

Endpoint *EndpointFactory::createEndpoint(const NetworkConfig &_config)
{
    Endpoint *iface;

    if (_config.type == "TCP Client")
        iface = new ClientTCPEndpoint();
    else if (_config.type == "TCP Server")
        iface = new ServerTCPEndpoint();

    iface->configure(_config);

    return iface;
}
