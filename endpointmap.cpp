/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#include "endpointmap.h"

#include "endpoint.h"

#include "dataviewer.h"

#include <QMenu>

EndpointMap::EndpointMap(QObject *parent) :
    QObject(parent)
{
    pEndpointA = 0;
    pEndpointB = 0;
    pMenu = 0;
    pView = 0;
}

EndpointMap::~EndpointMap()
{
    // delete view
    if (pView)
        pView->deleteLater();
    pView = 0;

    tearDown();

    if (pEndpointA)
      pEndpointA->deleteLater();
    if (pEndpointB)
      pEndpointB->deleteLater();

    pEndpointA = 0;
    pEndpointB = 0;

    if (pMenu)
    {
        QList<QAction *> endpointActions = pMenu->actions();
        for (int i = 0; i < endpointActions.count(); ++i)
        {
            QAction *act = endpointActions.takeLast();
            act->disconnect();
            delete act;
            act = 0;
        }
        delete pMenu;
        pMenu = 0;
    }
}

void EndpointMap::deleteAction()
{
    emit deleteMapping(mName);
}

bool EndpointMap::operator==(const EndpointMap &_other)
{
    return (pEndpointA == _other.pEndpointA
            && pEndpointB == _other.pEndpointB
            && mName == _other.mName);
}

void EndpointMap::setName(const QString &_name)
{
    mName = _name;
}

const QString &EndpointMap::name() const
{
    return mName;
}

void EndpointMap::appendMenu(QMenu *_menu)
{
    pMenu = _menu;
}

void EndpointMap::addEndpointPair(Endpoint *A, Endpoint *B)
{
    if (A != B) // do not map to the same item
    {
        pEndpointA = A;
        pEndpointB = B;
    }
}

void EndpointMap::setup()
{
    connect((QObject *)pEndpointA, SIGNAL(dataFromDevice(const QByteArray &)),
            (QObject *)pEndpointB, SLOT(toDevice(const QByteArray &)));
    connect((QObject *)pEndpointB, SIGNAL(dataFromDevice(const QByteArray &)),
            (QObject *)pEndpointA, SLOT(toDevice(const QByteArray &)));
}

void EndpointMap::tearDown()
{
    disconnect((QObject *)pEndpointA, SIGNAL(dataFromDevice(const QByteArray &)),
               (QObject *)pEndpointB, SLOT(toDevice(const QByteArray &)));
    disconnect((QObject *)pEndpointB, SIGNAL(dataFromDevice(const QByteArray &)),
               (QObject *)pEndpointA, SLOT(toDevice(const QByteArray &)));
}

void EndpointMap::createViewWindow()
{
    emit createNewViewer(mName);
}


void EndpointMap::unregisterViewer(DataViewer *_view)
{
    if (_view)
    {
        disconnect((QObject *)pEndpointA, SIGNAL(dataFromDevice(const QByteArray &)),
                   (QObject *)_view, SLOT(appendFromSource(const QByteArray &)));
        disconnect((QObject *)pEndpointB, SIGNAL(dataFromDevice(const QByteArray &)),
                   (QObject *)_view, SLOT(appendFromSource(const QByteArray &)));
    }
}

void EndpointMap::registerViewer(DataViewer *_view)
{
    if (_view)
    {
       pView = _view;
       connect((QObject *)pEndpointA, SIGNAL(dataFromDevice(const QByteArray &)),
               (QObject *)_view, SLOT(appendFromSource(const QByteArray &)));
       connect((QObject *)pEndpointB, SIGNAL(dataFromDevice(const QByteArray &)),
               (QObject *)_view, SLOT(appendFromSource(const QByteArray &)));
    }
}
