/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#include "clienttcpendpoint.h"

#include <QString>
#include <QTcpSocket>


ClientTCPEndpoint::ClientTCPEndpoint()
    : mHost(QHostAddress::Any)
    , mPort(5000)
{
  pSock = new QTcpSocket(this);

  connect(pSock, SIGNAL(readyRead()), this, SLOT(obtainData()));
  connect(pSock, SIGNAL(error(QAbstractSocket::SocketError)),
         this, SLOT(handleError(QAbstractSocket::SocketError)));
}


void ClientTCPEndpoint::start()
{
    if (pSock)
        pSock->connectToHost(mHost, mPort);
}


void ClientTCPEndpoint::configure(const NetworkConfig &_config)
{
    mHost = QHostAddress(_config.host);
    mPort = _config.port;
}

void ClientTCPEndpoint::toDevice(const QByteArray &_data)
{
    pSock->write(_data);
}

void ClientTCPEndpoint::obtainData()
{
    QByteArray buffer = pSock->readAll();
    emit dataFromDevice(buffer);
}

void ClientTCPEndpoint::handleError(QAbstractSocket::SocketError _err)
{
 qWarning("client error!! %d", pSock->error());
 pSock->errorString();
}
