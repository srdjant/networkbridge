/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#include <QByteArray>

#include "dataviewer.h"
#include "endpointmap.h"

DataViewer::DataViewer(QWidget *parent) :
    QTextEdit(parent)
{

}

DataViewer::~DataViewer()
{
 //   if (pMapper)
 //       pMapper->unregisterViewer(this);
}

void DataViewer::appendFromSource(const QByteArray &_data)
{
    append( QString(_data));
}

void DataViewer::setMapper(EndpointMap *_mapper)
{
    pMapper = _mapper;
    if (pMapper)
        pMapper->registerViewer(this);
}
