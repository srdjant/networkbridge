/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/
#ifndef ENDPOINTMAP_H
#define ENDPOINTMAP_H

#include <QObject>
#include <QString>

class Endpoint;
class QMenu;
class DataViewer;

class EndpointMap : public QObject
{

    Q_OBJECT

public:
    EndpointMap(QObject *parent = 0);
    virtual ~EndpointMap();

    void setName(const QString &_name);
    const QString &name() const;

    void addEndpointPair(Endpoint *A, Endpoint *B);

    void setup();
    void tearDown();

    void unregisterViewer(DataViewer *_view);
    void registerViewer(DataViewer *_view);

    void appendMenu(QMenu *_menu);

    bool operator==(const EndpointMap &_other);

signals:
    void deleteMapping(const QString &);
    void createNewViewer(const QString &);

public slots:
    void deleteAction();
    void createViewWindow();

private:
    Endpoint   *pEndpointA;
    Endpoint   *pEndpointB;
    QString     mName;
    QMenu      *pMenu;
    DataViewer *pView;
};

#endif // ENDPOINTMAP_H


