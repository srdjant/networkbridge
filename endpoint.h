/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#ifndef ENDPOINT_H
#define ENDPOINT_H

#include <QObject>
#include <QString>

class NetworkConfig
{
    public:
       QString type;
       QString host;
       ushort  port;
};

class Endpoint: public QObject
{
    Q_OBJECT

    public:
        virtual void configure(const NetworkConfig &_config) = 0;
        virtual void start() = 0;

    public slots:
        virtual void toDevice(const QByteArray &) = 0;

    signals:
        void dataFromDevice(const QByteArray &);

};

#endif // ENDPOINT_H
