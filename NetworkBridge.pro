TEMPLATE = app
INCLUDEPATH += .

# Input
HEADERS += endpoint.h \
    mainwindow.h \
    servertcpendpoint.h \
    clienttcpendpoint.h \
    endpointmap.h \
    newmappingdialog.h \
    endpointfactory.h \
    dataviewer.h
INTERFACES += mainwindow.ui
SOURCES += main.cpp \
    mainwindow.cpp \
    servertcpendpoint.cpp \
    clienttcpendpoint.cpp \
    endpointmap.cpp \
    newmappingdialog.cpp \
    endpointfactory.cpp \
    dataviewer.cpp
FORMS += mainwindow.ui \
    newmappingdialog.ui
QT += network
