/*
 Qt Network Bridge
 Copyright 2010 Srdjan Todorovic (todorovic.s@googlemail.com)
 Released under LGPL license.
*/

#ifndef ENDPOINTFACTORY_H
#define ENDPOINTFACTORY_H

#include <QString>

class ServerTCPEndpoint;
class ClientTCPEndpoint;
class Endpoint;
class NetworkConfig;

class EndpointFactory
{
    public:
        static Endpoint *createEndpoint(const NetworkConfig &_config);
};

#endif // ENDPOINTFACTORY_H
